﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HandMovement : MonoBehaviour
{

    private Animator animator;
    public static int label = 0;
    public static int previos_label = 0;
    private bool animation_started = false;
    private int updates = 0;

    private int[] gesture_code_array;
    private string gesture_code;
    private int gesture_count;
    public Text signal_text;

    
    // Use this for initialization
    void Start()
    {
        gesture_code_array = new int[5];
        gesture_code = "";
        gesture_count = 0;
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(gesture_count == 3)
        {
            gesture_code = gesture_code_array[0].ToString() + gesture_code_array[1].ToString() + gesture_code_array[2].ToString();
            switch(gesture_code)
            {
                case "421":      //it's ok
                    signal_text.text = "I'm ok";
                    gesture_count = 0;
                    break;
              
                case "423":    //help
                    signal_text.text = "Help";
                    gesture_count = 0;
                    break;

                case "402":     //user wants to make a signal but he is relaxing between gestures
                    break;

                case "420":     //user wants to make a signal but he is relaxing between gestures
                    break;

                default:
                    gesture_count = 0;
                    break;
            }
        }

        if (gesture_count == 4)
        {
            gesture_code = gesture_code_array[0].ToString() + gesture_code_array[1].ToString() + gesture_code_array[2].ToString() + gesture_code_array[3].ToString();
            switch (gesture_code)
            {
                case "4021":      //it's ok
                    signal_text.text = "I'm ok";
                    gesture_count = 0;
                    break;

                case "4201":      //it's ok
                    signal_text.text = "I'm ok";
                    gesture_count = 0;
                    break;

                case "4023":    //help
                    signal_text.text = "Help";
                    gesture_count = 0;
                    break;

                case "4203":    //help
                    signal_text.text = "Help";
                    gesture_count = 0;
                    break;

                case "4020":   //user wants to make a signal but he is relaxing between gestures
                    break;

                default:
                    gesture_count = 0;
                    break;
            }
        }

        if (gesture_count == 5)
        {
            gesture_code = gesture_code_array[0].ToString() + gesture_code_array[1].ToString() + gesture_code_array[2].ToString() + gesture_code_array[3].ToString() + gesture_code_array[4].ToString();
            switch (gesture_code)
            {
                case "40201":      //it's ok
                    signal_text.text = "I'm ok";
                    gesture_count = 0;
                    break;

                case "40203":    //help
                    signal_text.text = "Help";
                    gesture_count = 0;
                    break;

                default:
                    gesture_count = 0;
                    break;
            }
        }

        if (animation_started == false && previos_label != label)// am primit un label
        {
            switch (label)
            {
                case 0:
                    animator.SetBool("Relaxed", true);
                    animation_started = true;

                    gesture_code_array[gesture_count] = 0;
                    gesture_count++;
                    //Debug.Log("Relaxed animation started");
                    break;
                case 5:
                    animator.SetBool("Fist", true);
                    animation_started = true;

                    gesture_code_array[gesture_count] = 5;
                    gesture_count++;
                    //Debug.Log("Fist animation started");
                    break;
                case 6:
                    animator.SetBool("Spread", true);
                    animation_started = true;

                    gesture_code_array[gesture_count] = 6;
                    gesture_count++;
                    //Debug.Log("Spread animation started");
                    break;
                case 2:
                    animator.SetBool("WaveIn", true);
                    animation_started = true;

                    gesture_code_array[gesture_count] = 2;
                    gesture_count++;
                    //Debug.Log("Wave in animation started");
                    break;
                case 4:
                    animator.SetBool("WaveOut", true);
                    animation_started = true;

                    gesture_count = 0;
                    gesture_code_array[gesture_count] = 4;
                    gesture_count++;
                    //Debug.Log("Wave out animation started");
                    break;
                case 1:
                    animator.SetBool("WaveUp", true);
                    animation_started = true;

                    gesture_code_array[gesture_count] = 1;
                    gesture_count++;
                    //Debug.Log("Wave up animation started");
                    break;
                case 3:
                    animator.SetBool("WaveDown", true);
                    animation_started = true;

                    gesture_code_array[gesture_count] = 3;
                    gesture_count++;
                    //Debug.Log("Wave down animation started");
                    break;
                default:
                    break;
            }
        }
    }
    public void endAnimations(string message)
    {
        switch(message)
        {
            case "Relaxed animation ended":
                animator.SetBool("Relaxed", false);
                animation_started = false;
                previos_label = 0;
                //Debug.Log("Relaxed animation ended");
                break;

            case "Fist animation ended":
                animator.SetBool("Fist", false);
                animation_started = false;
                previos_label = 5;
                //Debug.Log("Fist animation ended");                                             
                break;

            case "Spread animation ended":
                animator.SetBool("Spread", false);
                animation_started = false;
                previos_label = 6;
                //Debug.Log("Spread animation ended");
                break;

            case "Wave in animation ended":
                animator.SetBool("WaveIn", false);
                animation_started = false;                  
                previos_label = 2;
                //Debug.Log("Wave in animation ended");
                break;                                                                                                                            
            case "Wave out animation ended":
                animator.SetBool("WaveOut", false);
                animation_started = false;
                previos_label = 4;
                //Debug.Log("Wave out animation ended");
                break;
            case "Wave up animation ended":
                animator.SetBool("WaveUp", false);
                animation_started = false;
                previos_label = 1;
                //Debug.Log("Wave up animation ended");
                break;
            case "Wave down animation ended":
                animator.SetBool("WaveDown", false);
                animation_started = false;
                previos_label = 3;
                //Debug.Log("Wave down animation ended");
                break;
            default:
                break;
        }
    }
    public void changeAnimation(int animation_number)
    {
        //Debug.Log("Button pressed");
        label = animation_number;
    }
 }
