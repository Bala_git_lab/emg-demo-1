﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartDemo : MonoBehaviour {

    public SpriteRenderer upb_logo_sprite;
    public SpriteRenderer speed_logo_sprite;

    public SpriteRenderer black_backround;
    public Canvas ip_canvas;
    public Canvas exit_canvas;


    private bool upb_logo_isactive;
    private bool first_displayed_upb_logo;

    private bool first_displayed_speed_logo = true;
    bool speed_logo_isactive;


    // Use this for initialization
    void Start()
    {
        upb_logo_isactive = true;
        first_displayed_upb_logo = true;

        speed_logo_isactive = false;
        first_displayed_speed_logo = false;
        speed_logo_sprite.color = new Color(speed_logo_sprite.color.r, speed_logo_sprite.color.g, speed_logo_sprite.color.b, 1);

        StartCoroutine(changeUpbLogoAlpha());
    }
	// Update is called once per frame
	void Update () {
        if (upb_logo_isactive == false)
        {
            speed_logo_isactive = true;
            first_displayed_speed_logo = true;
            speed_logo_sprite.gameObject.SetActive(true);
            StartCoroutine(changeSpeedLogoAlpha());
            enabled = false;
        }
	}
    IEnumerator changeUpbLogoAlpha()
    {
        while (upb_logo_sprite.color.a > 0)
        {
            if (first_displayed_upb_logo)
            {
                first_displayed_upb_logo = false;
                yield return new WaitForSeconds(1);
            }
            else
            {
                upb_logo_sprite.color = new Color(upb_logo_sprite.color.r, upb_logo_sprite.color.g, upb_logo_sprite.color.b, upb_logo_sprite.color.a - 0.016f);
                yield return new WaitForSeconds(0.033f);
            }
        }
        upb_logo_isactive = false;
    }
    IEnumerator changeSpeedLogoAlpha()
    {
        while (speed_logo_sprite.color.a > 0)
        {
            if (first_displayed_speed_logo)
            {
                first_displayed_speed_logo = false;
                yield return new WaitForSeconds(1);
            }
            else
            {
                speed_logo_sprite.color = new Color(speed_logo_sprite.color.r, speed_logo_sprite.color.g, speed_logo_sprite.color.b, speed_logo_sprite.color.a - 0.016f);
                yield return new WaitForSeconds(0.033f);
            }
        }
        speed_logo_isactive = false;
        black_backround.gameObject.SetActive(false);
        ip_canvas.gameObject.SetActive(true);
        exit_canvas.gameObject.SetActive(true);
    }
}


