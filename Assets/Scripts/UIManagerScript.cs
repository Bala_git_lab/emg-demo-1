﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManagerScript : MonoBehaviour {

    public InputField ip_adress_field;
	public void setIpAdress()
    {
        Client.WebServiceURL = ip_adress_field.text.ToString();
        ip_adress_field.text = "";
    }
}
